package com.app.Controller;

import com.app.Domain.Hotel;
import com.app.Receiver.Receiver;
import com.app.Sender.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//creating RestController
@RestController
public class Controller
{

    @Autowired
    Receiver receiver;


    @Autowired
    Sender sender;

    @GetMapping("/hotel")
    private void receiveRequest()
    {
        receiver.receive("r2c ");
    }

    @PostMapping("/hotel")
    private int saveStudent(@RequestBody Hotel hotel)
    {
        //sending request
        sender.send(""+
                hotel.getId()+"-"+
                hotel.getRoom()+"-"+
                hotel.getName()+"-"+
                hotel.getEmail());
        return 0;
    }
}
